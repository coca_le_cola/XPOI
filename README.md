# XPOI
基于Java POI开源组件，开发更适用于项目开发的开源组件

> 一、基于特定excel模板，导出excel文档（目前只支持.xls后缀的模板）

>> CS结构excel导出

```java
// 数据源
List<JSONObject> list = new LinkedList<JSONObject>();
JSONObject jsonObj = new JSONObject();
jsonObj.put("id", 1);
jsonObj.put("recipientName", 2);
jsonObj.put("recipientEmail", 3);
jsonObj.put("emailTitle", 4);
jsonObj.put("emailContent", 5);
jsonObj.put("state", 6);
jsonObj.put("date", 7);

JSONObject jsonObj1 = new JSONObject();
jsonObj1.put("id", 11);
jsonObj1.put("recipientName", 22);
jsonObj1.put("recipientEmail", 33);
jsonObj1.put("emailTitle", 44);
jsonObj1.put("emailContent", 55);
jsonObj1.put("state", 66);
jsonObj1.put("date", 77);

list.add(jsonObj);
list.add(jsonObj1);

WorkbookDesigner designer = new WorkbookDesigner();

// 模板路径
String templatePath = "D:\\Template.xls";
designer.open(templatePath);

// 添加数据源
designer.setDataSource(list);

// 添加自定义字段
designer.setDataSource("SenderEmail", "888888@qq.com");
designer.setDataSource("SenderDate", "2017-12-08");

designer.process();

// 导出excel
String saveFilePath = "D:\\Template11.xls";
designer.saveFile(saveFilePath);
```

>> BS结构excel下载

```java
// 数据源
List<JSONObject> list = new LinkedList<JSONObject>();
JSONObject jsonObj = new JSONObject();
jsonObj.put("id", 1);
jsonObj.put("recipientName", 2);
jsonObj.put("recipientEmail", 3);
jsonObj.put("emailTitle", 4);
jsonObj.put("emailContent", 5);
jsonObj.put("state", 6);
jsonObj.put("date", 7);

JSONObject jsonObj1 = new JSONObject();
jsonObj1.put("id", 11);
jsonObj1.put("recipientName", 22);
jsonObj1.put("recipientEmail", 33);
jsonObj1.put("emailTitle", 44);
jsonObj1.put("emailContent", 55);
jsonObj1.put("state", 66);
jsonObj1.put("date", 77);

list.add(jsonObj);
list.add(jsonObj1);

WorkbookDesigner designer = new WorkbookDesigner();

// 模板路径
String templatePath = "D:\\Template.xls";
designer.open(templatePath);

// 添加数据源
designer.setDataSource(list);

// 添加自定义字段
designer.setDataSource("SenderEmail", "888888@qq.com");
designer.setDataSource("SenderDate", "2017-12-08");

designer.process();

ByteArrayOutputStream os = designer.getOs();
byte[] content = os.toByteArray();
InputStream is = new ByteArrayInputStream(content);
// 设置response参数，可以打开下载页面
response.reset();// response 为 HttpServletResponse对象

String title = "ceshi";
String fileName = new String((title + ".xls").getBytes(), "iso-8859-1");
response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
response.setContentLength(content.length);
ServletOutputStream outputStream = response.getOutputStream();
BufferedInputStream bis = new BufferedInputStream(is);
BufferedOutputStream bos = new BufferedOutputStream(outputStream);
byte[] buff = new byte[8192];
int bytesRead;
while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
	bos.write(buff, 0, bytesRead);
}
bis.close();
bos.close();
outputStream.flush();
outputStream.close();
```

详细介绍及相关文件下载：[XPOI——特定模板excel文件导出](http://www.yangda.xin/topic/9.html)