package xin.yangda.xpoi.test;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.alibaba.fastjson.JSONObject;

import xin.yangda.xpoi.xls.WorkbookDesigner;

/**
 * 根据特定Excel导出Excel文档测试
 * 
 * @author izifeng
 * @version 1.0
 * @date 2017-12-11 11:49
 * @site https://gitee.com/izifeng/XPOI.git
 *
 */
public class ExcelExport {

	private ExcelExport() {

	}

	public static void main(String[] args) throws IOException {
		// 数据源
		List<JSONObject> list = new LinkedList<>();
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("id", 1);
		jsonObj.put("recipientName", 2);
		jsonObj.put("recipientEmail", 3);
		jsonObj.put("emailTitle", 4);
		jsonObj.put("emailContent", 5);
		jsonObj.put("state", 6);
		jsonObj.put("date", 7);

		JSONObject jsonObj1 = new JSONObject();
		jsonObj1.put("id", 11);
		jsonObj1.put("recipientName", 22);
		jsonObj1.put("recipientEmail", 33);
		jsonObj1.put("emailTitle", 44);
		jsonObj1.put("emailContent", 55);
		jsonObj1.put("state", 66);
		jsonObj1.put("date", 77);

		list.add(jsonObj);
		list.add(jsonObj1);

		WorkbookDesigner designer = new WorkbookDesigner();

		// 模板路径
		String templatePath = "resources/ExcelExportTemplate.xls";
		designer.open(templatePath);

		// 添加数据源
		designer.setDataSource(list);

		// 添加自定义字段
		designer.setDataSource("SenderEmail", "888888@qq.com");
		designer.setDataSource("SenderDate", "2017-12-08");

		// excel数据加工
		designer.process();

		// 导出excel
		String saveFilePath = "D:\\ExcelExportTest.xls";
		designer.saveFile(saveFilePath);
	}
}
